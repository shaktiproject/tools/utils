# Version update scripts for GitLab

1. The python script available here is used to update the versions of various repos
used within the shakti project.

2. The script follows the classical [semantic-versioning](https://semver.org/).

3. This script uses the gitlab API to update the versions and badges

4. This will only work on merge-requests and will need the merge-request to have
   one of the following labels:
   * bump-major
   * bump-minor
   * bump-patch (default if no label is assigned)

5. As part of the description the script will capture all the issues that were
   closed with this merge-request and populates the title in the
   tag-description.

6. The script will also update the badge `version` if it exists with the latest
   version number.

7. Example : [C-class](https://gitlab.com/shaktiproject/cores/c-class)



