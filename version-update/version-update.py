
import os
import re
import sys
import semver
import subprocess
import gitlab
import shlex

change_log = '''
##### CHANGELOG:

'''

def verify_env_var_presence(name):
    if name not in os.environ:
        raise Exception("Expected the following environment variable to be set: {name}")

def bump():
    
    # Collect environment variables
    username = os.environ["NPA_USERNAME"]
    password = os.environ["NPA_PASSWORD"]
    project_url = os.environ['CI_PROJECT_URL']
    project_path = os.environ['CI_PROJECT_PATH']
    project_id = os.environ['CI_PROJECT_ID']
    gitlab_url = project_url.split(project_path)[0]

    print('Project URL  : ' + str(project_url))
    print('Project Path : ' + str(project_path))
    print('Project ID   : ' + str(project_id))
    print('Gitlab URL   : ' + str(gitlab_url))


    # get relevant project
    gl = gitlab.Gitlab(gitlab_url, private_token=password)
    gl.auth()
    project = gl.projects.get(project_id)
   
    # get badge list
    badge_list = project.badges.list()

    # get the latest tag
    latest_tag = project.tags.list()[0].name
    if '-' in latest_tag:
        latest_tag = '1.0.0'

    # Get latest Commit message
    commit_message = project.commits.list()[0].message
    print(commit_message)

    # extracting merge request id from commit
    merge_req_id = re.search(r'(\S*\/\S*!)(\d+)', commit_message, \
                        re.M|re.I).group(2)    
    print(merge_req_id)
    # get mergerequest and its labels
    merge_request = project.mergerequests.get(merge_req_id)
    merge_labels = merge_request.labels

    # generate tag release notes
    merge_description = merge_request.description
    print(merge_description)
    merge_issues = re.findall( r'\s*Closes\s*#(\d+.*?)', merge_description)
    print(merge_issues)
#    if len(merge_issues) == 0 :
#      raise Exception('No issues associated with Merge')

    tag_release = change_log
    for i in merge_issues:
      tag_release = tag_release + '* Resolved issue[#'+i+']: '+ \
                              str(project.issues.get(i).title) +\
                              str('\n')

    # generate new version from semver 
    if "bump-major" in merge_labels:
        version = semver.bump_major(latest_tag)
    elif "bump-minor" in merge_labels:
        version = semver.bump_minor(latest_tag)
    else:
        version = semver.bump_patch(latest_tag)
    print('New Tag      : ' + str(version))

    # create new tag
    tags = project.tags.create({'tag_name': version, 'ref': 'master'})
    tags.set_release_description(tag_release)
   
    # update badge
    if len(badge_list) != 0:
        for b in badge_list:
            if 'version' in b.image_url:
                b.image_url = 'https://img.shields.io/badge/version-'+version+'-red.svg'
                b.save()

def main():
    env_list = ["CI_REPOSITORY_URL", "CI_PROJECT_ID", "CI_PROJECT_URL", "CI_PROJECT_PATH", "NPA_USERNAME", "NPA_PASSWORD"]
    for e in env_list:
      [verify_env_var_presence(e) for e in env_list]

    bump()
    
    return 0


if __name__ == "__main__":
    sys.exit(main())
