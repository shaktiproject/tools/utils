# Utility scripts

Generic utility scripts or programs used across Shakti

# List
|   util   |               description               |              usage             |
|:--------:|:---------------------------------------:|:------------------------------:|
| gen_diag | Generates block diagram from bsv source | [gen_diag.md](doc/gen_diag.md) |
| bsv-sphinx-plugin | Parses BSV and extracts documentation   | WIP                            |
| version-update| A GitLab-API based python script to automate version maintenance of repos | [README](version-update/README.md)|
