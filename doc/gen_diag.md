# gen_diag

## Setup
```sh
$ sudo pip3 install colorlog pyyaml
$ sudo apt-get install graphviz
```

## Man
```sh
 $ python3.6 gen_diag.py --help
usage: gen_diag.py [-h] -f FILENAME -d DIRNAME

Generate png block diagram from BSV source

optional arguments:
  -h, --help            show this help message and exit
  -f FILENAME, --filename FILENAME
                        bsv source file; "all" for all files in dirname
  -d DIRNAME, --dirname DIRNAME
                        bsv directory source

```

## Usage
Create a directory $PWD/core with all bsv files
```sh
$ python3.6 gen_diag.py --filename=all --dirname=$PWD/core

# eg:
$  python gen_diag.py  -f all -d 'e-class/src/core' --getall
```
