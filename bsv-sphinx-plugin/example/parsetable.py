import os
import sys
import re
import string as string
import pytablewriter

def reindent(s, numSpaces):
    s = s.split('\n')
    s = [re.sub('^', ' '*numSpaces, line, re.M|re.S) for line in s]
    s = "\n".join(s)
    return s

pkg = 'icache'
instance = 'mkicache'

prelude = [
    'Connectable',
    'FIFOF',
    'BUtils',
    'GetPut',
    'FIFO',
    'SpecialFIFOs',
    'Clocks',
    'DReg',
    'FIFOLevel',
    'Vector',
    'Counter',
    'ConfigReg',
    'Assert',
    'BRAMCore',
    'BRAM',
    'RegFile',
    'UniqueWrappers',
    'LFSR',
    'Randomizable',
    'StmtFSM',
    'List',
    'ClientServer',
    'Probe',
    'OVLAssertions',
    'CBus',
    'ModuleCollect',
    'RevertReg',
    'MIMO',
    'List',
    'OInt',
    'Memory',
    'Cntrs',
    'GrayCounter',
    'CompletionBuffer',
    'DefaultValue',
    'TieOff',
    'ZBus',
    'CRC',
    'Real',
    'Gearbox',
    'AlignedFIFOs'
]


moduletemplate = '''
Description
{0}
'''

ruletemplate = '''
* `{0} <{4}>`_

    Description
{1}
'''

def clean_up_text(text):
    text = text.replace("\\", "")
    text = text.replace("\t", " ")
    text = text.replace("  ", " ")
    return text


#def extract_rules(bsv, sched):
#    '''
#    Extracts the following for each Rule:
#        1. Rule Name
#        2. Description of each rule
#        3. Predicates for each rule
#    '''
#    ruleextract =\
#    re.findall(r'\/\*\s*doc\s*:\s*rule\s*:[\n]*(.*?)\*\/\n\s*rule\s(.*?)\s*[\(;]', bsv, re.M|re.S)
##    re.findall(r'^\s*\(\*\s*doc\s*=\s*\"\s*rule\s*:\n(.*?)\"\s*\*\)\n(?=\s*rule\s*(.*?)\s*[\(;])', bsv, re.M|re.S)
#    for r in ruleextract:
#        pred = re.findall(r'^\s*Rule:\s*{0}\s*Predicate:\s*(.*?)Blocking rules: (.*?)\s[Rule|Logical]'.format(r[1]) , sched, re.M|re.S)[0]
#        lineno = get_lineno(bsv, 'rule '+r[1])
#        print(ruletemplate.format(r[1], reindent(r[0],6),
#            reindent(pred[0],6), pred[1], lineno))

def extract_rules(bsv, sched):
    '''
    Extracts the following for each Rule:
        1. Rule Name
        2. Description of each rule
        3. Predicates for each rule
    '''
    writer = pytablewriter.RstGridTableWriter()
    writer.table_name = ""
    writer.headers = ["Name", "Description"]
    matrix = []
    ruleextract =\
    re.findall(r'\/\*\s*doc\s*:\s*rule\s*:[\n]*(.*?)\*\/\n\s*rule\s(.*?)\s*[\(;]', bsv, re.M|re.S)
#    re.findall(r'^\s*\(\*\s*doc\s*=\s*\"\s*rule\s*:\n(.*?)\"\s*\*\)\n(?=\s*rule\s*(.*?)\s*[\(;])', bsv, re.M|re.S)
    for r in ruleextract:
        desc, name = r
        pred = re.findall(r'^\s*Rule:\s*{0}\s*Predicate:\s*(.*?)Blocking rules: (.*?)\s[Rule|Logical]'.format(name) , sched, re.M|re.S)[0]
        lineno = get_lineno(bsv, 'rule '+name)
        name = '`'+name+' <'+lineno+'>`_'
        matrix.append([re.sub('\[[^\]]+\]','',name), #pred[0], pred[1],  
            desc])
    
    writer.value_matrix = matrix
    writer.write_table()

def get_reg_resetval(reg):
    ''' 
    Extract the reset value of the register
    '''
    if 'mkRegU' in reg:
        return 'undefined'
    if 'mkReg' in reg:
        return re.findall(r'mkReg\((.+?)\)$',reg, re.M|re.S)[0]
    if 'mkCReg' in reg:
        return re.findall(r'mkCReg\(\s*\d\s*,\s*(.+?)\)$', reg, re.M|re.S)[0]
    else:
        return 'none'

def extract_regs(bsv):
    '''
    Extract the following for each Reg:
        1. Register Name
        2. Register Data Type
        3. Reset value if any
    '''
    x =\
    re.findall(r'\/\*\s*doc\s*:\s*reg\s*:[\n]*(.*?)\s*\*\/\n\s*Reg#\(\s*(.*?)\)\s+(.*?)\s*<-\s*(.*?);', bsv, re.M|re.S)
    writer = pytablewriter.RstGridTableWriter()
    writer.table_name = ""
    writer.headers = ["Name", "Data Type", "Reset Value", "Description"]
    matrix = []
    for r in x:
        desc, datatype, name, resetval = r
        resetvalue = get_reg_resetval(resetval)
        lineno = get_lineno(bsv, name)
        name = '`'+name+' <'+lineno+'>`_'
        matrix.append([re.sub('\[[^\]]+\]','',name), datatype.replace('`',''), 
            resetvalue,  reindent(desc, 6)])
    
    writer.value_matrix = matrix
    writer.write_table()

def get_wire_defaultval(reg):
    ''' 
    Extract the default value of the Wires (if any)
    '''
    if 'mkDWire' in reg:
        return re.findall(r'mkDWire\((.+?)\)$',reg, re.M|re.S)[0]
    else:
        return 'none'

def extract_wires(bsv):
    '''
    Extract the following for each Wire:
        1. Wire Name
        2. Wire Data Type
        3. Default value if any
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*wire\s*:[\n]*(.*?)\s*\*\/\n\s*Wire#\(\s*(.*?)\)\s+(.*?)\s*<-\s*(.*?);', bsv, re.M|re.S)
    writer = pytablewriter.RstGridTableWriter()
    writer.table_name = ""
    writer.headers = ["Name", "Data Type", "Default Value", "Description"]
    matrix = []
    for r in x:
        desc, datatype, name, resetval = r
        resetvalue = get_wire_defaultval(resetval)
        lineno = get_lineno(bsv, name)
        name = '`'+name+' <'+lineno+'>`_'
        matrix.append([re.sub('\[[^\]]+\]','',name), datatype.replace('`',''), resetvalue,  desc])
    
    writer.value_matrix = matrix
    writer.write_table()

def extract_fifodepth(fifo):
    '''
    Extract the fifo module type and the depth of the fifo
    '''
    name, size = re.findall(r'(mk.*FIFO.*?)\s*\((.*?)\)', fifo, re.M|re.S)[0]
    if size is '':
        size = 1
    return name, size

def extract_fifos(bsv):
    '''
    Extract the following for each FIFO
    1. FIFO instance name
    2. FIFO Data type
    3. FIFO Module 
    4. FIFO Size
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*fifo\s*:[\n]*(.*?)\s*\*\/\n\s*FIFO[F]#\(\s*(.*?)\)\s+(.*?)\s*<-\s*(.*?);', bsv, re.M|re.S)
    writer = pytablewriter.RstGridTableWriter()
    writer.table_name = ""
    writer.headers = ["Name", "Data Type", "FIFO-Type", "Depth", "Description"]
    matrix = []
    for r in x:
        desc, datatype, name, typesize = r
        typename, size = extract_fifodepth(typesize)
        lineno = get_lineno(bsv, name)
        name = '`'+name+' <'+lineno+'>`_'
        matrix.append([re.sub('\[[^\]]+\]','',name), datatype.replace('`',''), 
                        typename, size, desc])
    
    writer.value_matrix = matrix
    writer.write_table()

def extract_funcs(bsv):
    '''
    Extract the following for each function
    1. function name
    2. function Return Type
    3. function Args (if any)

    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*func\s*:[\n]*(.*?)\s*\*\/\n\s*function\s*(.*?)\s*(fn_.*?)\((.*?)\)\s*[=;]', bsv, re.M|re.S)
    writer = pytablewriter.RstGridTableWriter()
    writer.table_name = ""
    writer.headers = ["Name", "Return Type", "Arguments", "Description"]
    matrix = []
    for r in x:
        desc, returnval, name, args = r
        lineno = get_lineno(bsv, r[2])
        name = '`'+name+' <'+lineno+'>`_'
        matrix.append([re.sub('\[[^\]]+\]','',name), returnval.replace('`',''), 
                        args.replace('`',''), desc])
    
    writer.value_matrix = matrix
    writer.write_table()

def extract_subifc(bsv):
    ''' 
    Extract the following for each subinterface used:
    1. Interface Name
    2. Interface Data type
    3. Description
    '''
    writer = pytablewriter.RstGridTableWriter()
    writer.table_name = ""
    writer.headers = ["Name", "Data Type", "Description"]
    matrix = []

    x = re.findall(r'\/\*\s*doc\s*:\s*subifc\s*:[\n]*(.*?)\s*\*\/\n\s*interface\s*(.*?)\s+(.*?)[=;]', bsv, re.M|re.S)
    for r in x:
        desc, datatype, name = r
        lineno = get_lineno(bsv, name)
        name = '`'+name+' <'+lineno+'>`_'
        matrix.append([re.sub('\[[^\]]+\]','',name), datatype.replace('`',''), 
                    desc])
    
    writer.value_matrix = matrix
    writer.write_table()

def extract_methods(bsv):
    '''
    Extract the following for each method
    1. Method Name
    2. Method type
    3. method arguments (if any)
    4. method return type in case of ActionValue
    '''
    writer = pytablewriter.RstGridTableWriter()
    writer.table_name = ""
    writer.headers = ["Name", "Method-Type", "Arguments", "Return Type", "Description"]
    matrix = []
    x = re.findall(r'\/\*\s*doc\s*:\s*method\s*:[\n]*(.*?)\s*\*\/\n\s*method\s*(.*?)\s+(.*?)\s*\(\s*(.*?)\)\s*;', bsv, re.M|re.S)
    for r in x:
        desc, methodtype, name, arguments = r
        returntype = 'none'
        lineno = get_lineno(bsv, name)
        name = '`'+name+' <'+lineno+'>`_'
        if arguments is '':
            arguments = 'none'
        if 'ActionValue' in methodtype:
            returntype = re.findall(r'ActionValue#\((.*?)\)$', methodtype, re.M|re.S)[0]
            methodtype = 'ActionValue'

        matrix.append([re.sub('\[[^\]]+\]','',name), methodtype,
            arguments.replace('`',''), returntype.replace('`',''), desc])
    
    writer.value_matrix = matrix
    writer.write_table()

def extract_imports(bsv):
    '''
    Extract the list of library and project imports
    '''
    writer = pytablewriter.RstGridTableWriter()
    writer.table_name = ""
    writer.headers = ["BSV Library Imports", "Project Imports"]

    x = re.findall(r'^\s*import\s*(.*?)\s*::\s*\*\s*;', bsv, re.M|re.S)
    matrix = [[""], [""]]
    count = 1
    for r in x:
        if r in prelude:
            matrix[0][0] = matrix[0][0]+' '+r+','
        else:
            matrix[1][0] =  matrix[1][0]+' '+r+','
    writer.value_matrix = matrix
    writer.write_table()

def extract_notes(bsv):
    '''
    Extract notes if any
    '''
    x = re.findall(r'\(\*\s*doc\s*=\s*\"\s*note\s*:\s*(.*?)\"\s*\*\)', bsv, re.M|re.S)
    count = 1
    for r in x:
        print(str(count)+'. '+r)

def extract_module(bsv):
    ''' 
    Extract module description
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*module\s*:\s*\n(.*?)\*\/\n(?=\s*module)', bsv, re.M|re.S)
    for r in x:
        print((moduletemplate.format(r)))

def extract_overview(bsv):
    ''' 
    Extract overview
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*overview\s*:\s*\n(.*?)\*\/', bsv, re.M|re.S)
    for r in x:
        print(r)

def get_lineno(bsv, text):
    global srclink
    lineno = 1
    x = bsv.find(text)
    if x is not -1:
        for i in range(0, len(bsv)):
            if '\n' in bsv[i]:
                lineno = lineno + 1
            if i == x:
                return srclink+'L'+str(lineno)


if __name__ == '__main__' :

    global srclink
    srclink = ''
    bsvfile = open(pkg+'.bsv', 'r')
    bsv = bsvfile.read()

    schedule = open(instance+'.sched' , 'r')
    sched = schedule.read()
    use = open(instance+'.use', 'r')
    verilog = open(instance+'.v' , 'r')

    print('###################\nModule: '+instance+'\n###################')
    extract_overview(bsv)
    extract_module(bsv)
    print('Code Documentation\n------------------')
    print('\nLibrary Imports'+'\n^^^^^^^^^^^^^^^^^^')
    extract_imports(bsv)
    print('\nRegister Instances'+'\n^^^^^^^^^^^^^^^^^^^^')
    extract_regs(bsv)
    print('\nWire Instances'+'\n^^^^^^^^^^^^^^^^^^^^')
    extract_wires(bsv)
    print('\nFIFO Instances'+'\n^^^^^^^^^^^^^^^^^^^^')
    extract_fifos(bsv)
    print('\nLocal functions'+'\n^^^^^^^^^^^^^^^^^^^')
    extract_funcs(bsv)
    print('\nSub-Interface'+'\n^^^^^^^^^^^^^^^^^^^^^^^^^^')
    extract_subifc(bsv)
    print('\nMethods'+'\n^^^^^^^^^^^^^^^^^^^^^^^^^^')
    extract_methods(bsv)
    print('\nRule Instances'+'\n^^^^^^^^^^^^^^^^^^^^')
    extract_rules(bsv, sched)
    print('\nNotes'+'\n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^')
    extract_notes(bsv)

    bsvfile.close()
    schedule.close()
    use.close()
    verilog.close()
