import os
import sys
import re
import string as string

def reindent(s, numSpaces):
    s = s.split('\n')
    s = [re.sub('^', ' '*numSpaces, line, re.M|re.S) for line in s]
    s = "\n".join(s)
    return s

pkg = 'icache'
instance = 'mkicache'
boolmacro_list = []
valuemacro_list = []
defaultmacros = ['ifdef', 'logLevel(', 'endif','else', 'include']

prelude = [
    'Connectable',
    'FIFOF',
    'BUtils',
    'GetPut',
    'FIFO',
    'SpecialFIFOs',
    'Clocks',
    'DReg',
    'FIFOLevel',
    'Vector',
    'Counter',
    'ConfigReg',
    'Assert',
    'BRAMCore',
    'BRAM',
    'RegFile',
    'UniqueWrappers',
    'LFSR',
    'Randomizable',
    'StmtFSM',
    'List',
    'ClientServer',
    'Probe',
    'OVLAssertions',
    'CBus',
    'ModuleCollect',
    'RevertReg',
    'MIMO',
    'List',
    'OInt',
    'Memory',
    'Cntrs',
    'GrayCounter',
    'CompletionBuffer',
    'DefaultValue',
    'TieOff',
    'ZBus',
    'CRC',
    'Real',
    'Gearbox',
    'AlignedFIFOs'
]

moduletemplate = '''
Description
{0}
'''

ruletemplate = '''
* `{0} <{4}>`_

 - **Description**:{1}
 - **Blocking Rules/Methods**: {3}
 - **Predicate**:

     .. code-block:: 

     {2}

'''

regtemplate = '''
{5}. `{0} <{4}>`_

  - **Data Type**: ``{1}``
  - **Reset Value**: ``{2}``
  - **Description**: {3}
'''

wiretemplate = '''
{5}. `{0} <{4}>`_

  - **Data Type**: ``{1}``
  - **Default Value**: ``{2}``
  - **Description**: {3}
'''

fifotemplate = '''
{6}. `{0} <{5}>`_

 - **Data Type** : ``{1}``
 - **Module Type** : ``{2}``
 - **Depth of FIFO** : {3}
 - **Description**: {4}

'''

functemplate = '''
* `{0} <{4}>`_
    
    Description
{3}

    Return type
        ``{1}``

    Arguments
        ``{2}``

'''

subifctemplate = '''
{4} `{0} <{3}>`_
    
 - **Data Type**: ``{1}``
 - **Description**:{2}

'''

methodtemplate = '''
{6}. `{0} <{5}>`_
    
 - **Method Type**: ``{1}``
 - **Module Arguments**: ``{2}``
 - **Method Return Type**: ``{3}``
 - **Description**: {4}

'''

def clean_up_text(text):
    text = text.replace("\\", "")
    text = text.replace("\t", " ")
    text = text.replace("  ", " ")
    return text


def extract_rules(bsv, sched):
    '''
    Extracts the following for each Rule:
        1. Rule Name
        2. Description of each rule
        3. Predicates for each rule
    '''
    ruleextract =\
    re.findall(r'\/\*\s*doc\s*:\s*rule\s*:[\n]*(.*?)\*\/\n\s*rule\s(.*?)\s*[\(;]', bsv, re.M|re.S)
#    re.findall(r'^\s*\(\*\s*doc\s*=\s*\"\s*rule\s*:\n(.*?)\"\s*\*\)\n(?=\s*rule\s*(.*?)\s*[\(;])', bsv, re.M|re.S)
    if len(ruleextract) != 0 :
        print('\nRule Instances'+'\n--------------------')
    for r in ruleextract:
        pred = re.findall(r'^\s*Rule:\s*{0}\s*Predicate:\s*(.*?)Blocking rules: (.*?)\s[Rule|Logical]'.format(r[1]) , sched, re.M|re.S)[0]
        lineno = get_lineno(bsv, 'rule '+r[1])
        print(ruletemplate.format(r[1], r[0].replace('\n',''),
            reindent(pred[0],6), pred[1], lineno))

def get_reg_resetval(reg):
    ''' 
    Extract the reset value of the register
    '''
    if 'mkRegU' in reg:
        return 'undefined'
    if 'mkReg' in reg:
        return re.findall(r'mkReg\((.+?)\)$',reg, re.M|re.S)[0]
    if 'mkCReg' in reg:
        return re.findall(r'mkCReg\(\s*\d\s*,\s*(.+?)\)$', reg, re.M|re.S)[0]
    else:
        return 'none'

def extract_regs(bsv):
    '''
    Extract the following for each Reg:
        1. Register Name
        2. Register Data Type
        3. Reset value if any
    '''
    x =\
    re.findall(r'\/\*\s*doc\s*:\s*reg\s*:[\n]*(.*?)\s*\*\/\n\s*Reg#\(\s*(.*?)\)\s+(.*?)\s*<-\s*(.*?);', bsv, re.M|re.S)
    if len(x) != 0:
        print('\nRegister Instances'+'\n--------------------')
    count = 0;
    for r in x:
        count = count + 1
        desc, datatype, name, resetval = r
        resetvalue = get_reg_resetval(resetval)
        lineno = get_lineno(bsv, name)
        print(regtemplate.format(re.sub('\[[^\]]+\]','',name), datatype, resetvalue, 
                desc.replace('\n',''), lineno, count))

def get_wire_defaultval(reg):
    ''' 
    Extract the default value of the Wires (if any)
    '''
    if 'mkDWire' in reg:
        return re.findall(r'mkDWire\((.+?)\)$',reg, re.M|re.S)[0]
    else:
        return 'none'

def extract_wires(bsv):
    '''
    Extract the following for each Wire:
        1. Wire Name
        2. Wire Data Type
        3. Default value if any
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*wire\s*:[\n]*(.*?)\s*\*\/\n\s*Wire#\(\s*(.*?)\)\s+(.*?)\s*<-\s*(.*?);', bsv, re.M|re.S)
    if len(x) != 0:
        print('\nWire Instances'+'\n--------------------')
    count = 0
    for r in x:
        count = count + 1
        desc, datatype, name, resetval = r
        resetvalue = get_wire_defaultval(resetval)
        lineno = get_lineno(bsv, name)
        print(wiretemplate.format(re.sub('\[[^\]]+\]','',name), datatype,
            resetvalue, desc.replace('\n',''), lineno, str(count)))

def extract_fifodepth(fifo):
    '''
    Extract the fifo module type and the depth of the fifo
    '''
    name, size = re.findall(r'(mk.*FIFO.*?)\s*\((.*?)\)', fifo, re.M|re.S)[0]
    if size is '':
        size = 1
    return name, size

def extract_fifos(bsv):
    '''
    Extract the following for each FIFO
    1. FIFO instance name
    2. FIFO Data type
    3. FIFO Module 
    4. FIFO Size
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*fifo\s*:[\n]*(.*?)\s*\*\/\n\s*FIFO[F]#\(\s*(.*?)\)+\s*(.*?)\s*<-\s*(.*?);', bsv, re.M|re.S)
    if len(x) != 0:
        print('\nFIFO Instances'+'\n--------------------')
    count = 0
    for r in x:
        count = count + 1
        desc, datatype, name, fifoinfo = r
        fifotype, size = extract_fifodepth(fifoinfo)
        lineno = get_lineno(bsv, name)
        print(fifotemplate.format(re.sub('\[[^\]]+\]','',name), datatype,
            fifotype,  size, desc.replace('\r',''), lineno, count))

def extract_funcs(bsv):
    '''
    Extract the following for each function
    1. function name
    2. function Return Type
    3. function Args (if any)

    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*func\s*:[\n]*(.*?)\s*\*\/\n\s*function\s*(.*?)\s*(fn_.*?)\((.*?)\)\s*[=;]', bsv, re.M|re.S)
    if len(x) != 0:
        print('\nLocal functions'+'\n-------------------')
    for r in x:
        lineno = get_lineno(bsv, r[2])
        print(functemplate.format(r[2], r[1], re.sub('\s+',' ',r[3]).rstrip(), reindent(r[0], 6), lineno))

def extract_ifc(bsv):
    ''' 
    Extract the following for each subinterface used:
    1. Interface Name
    2. Interface Data type
    3. Description
    '''

    subifc = re.findall(r'\/\*\s*doc\s*:\s*subifc\s*:[\n]*(.*?)\s*\*\/\n\s*interface\s*(.*?)\s+(.*?)[=;]', bsv, re.M|re.S)
    method = re.findall(r'\/\*\s*doc\s*:\s*method\s*:[\n]*(.*?)\s*\*\/\n\s*method\s*(.*?)\s+([ma mv mav].*?)\s*\(\s*(.*?)\)\s*;', bsv, re.M|re.S)
    
    if len(subifc) != 0 or len(method) != 0:
        print('\nInterface'+'\n--------------------------')
    count = 0
    for r in subifc:
        count = count + 1
        lineno = get_lineno(bsv, r[2])
        print(subifctemplate.format(r[2], r[1], reindent(r[0], 4), lineno, count))
    for r in method:
        count = count + 1
        desc, methodtype, name, arguments = r
        returntype = 'none'
        lineno = get_lineno(bsv, name)
        if arguments is '':
            arguments = 'none'
        if 'ActionValue' in methodtype:
            returntype = re.findall(r'ActionValue#\((.*?)\)$', methodtype, re.M|re.S)[0]
            methodtype = 'ActionValue'
        print(methodtemplate.format(name, methodtype, arguments, returntype, 
            desc.replace('\n',''), lineno, count))    

def extract_imports(bsv):
    '''
    Extract the list of library and project imports
    '''
    x = re.findall(r'^\s*import\s*(.*?)\s*::\s*\*\s*;', bsv, re.M|re.S)
    print(' - **BSV Library Imports**:\n')
    count = 1
    for r in x:
        if r in prelude:
            print('   * '+r)
            count = count+1
    print('\n - **Porject Library Imports**:\n')
    count = 1
    for r in x:
        if r not in prelude:
            print('   * '+r)
            count = count+1

def extract_notes(bsv):
    '''
    Extract notes if any
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*note\s*:\s*(.*?)\s*\*\/', bsv, re.M|re.S)
    count = 1
    for r in x:
        print('.. note:: '+r)

def extract_module(bsv):
    ''' 
    Extract module description
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*module\s*:\s*\n(.*?)\*\/\n(?=\s*module)', bsv, re.M|re.S)
    for r in x:
        print((moduletemplate.format(r)))

def extract_compile_macros(bsv):
    '''
    Extract the list of the ifdef macros that can be enabled/disabled at compile
    time
    '''
    global boolmacro_list
    global valuemacro_list
    boolean_macros = re.findall(r'`ifdef\s*(.*?)[\s$](.*?)`endif', bsv, re.M|re.S)
    value_macros = re.findall(r'`(.*?)[\[,\s\)\];]',bsv,re.M|re.S)
    if len(boolean_macros) != 0 or len(value_macros) != 0:
        print('\nCompile Macros'+'\n--------------------------')

    if len(boolean_macros) != 0: 
        print('\nBoolean Macros'+'\n^^^^^^^^^^^^^^^^^^')
        for r in boolean_macros:
            macro, text = r
            if macro not in boolmacro_list:
                print(' * ' + macro)
                boolmacro_list.append(macro)

    if len(value_macros) != 0: 
        print('\nValue Based Macros'+'\n^^^^^^^^^^^^^^^^^^')
        for macro in value_macros:
            if '`' not in macro and macro not in defaultmacros:
                if macro not in valuemacro_list:
                    print(' * ' + macro)
                    valuemacro_list.append(macro)

def extract_overview(bsv):
    ''' 
    Extract overview
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*overview\s*:\s*\n(.*?)\*\/', bsv, re.M|re.S)
    for r in x:
        print(r)

def get_lineno(bsv, text):
    global srclink
    lineno = 1
    x = bsv.find(text)
    if x is not -1:
        for i in range(0, len(bsv)):
            if '\n' in bsv[i]:
                lineno = lineno + 1
            if i == x:
                return srclink+'L'+str(lineno)


if __name__ == '__main__' :

    global srclink
    srclink = 'https://gitlab.com/shaktiproject/cores/e-class/blob/master/src/core/'+pkg+'.bsv#'
    bsvfile = open(pkg+'.bsv', 'r')
    bsv = bsvfile.read()

    schedule = open(instance+'.sched' , 'r')
    sched = schedule.read()
    use = open(instance+'.use', 'r')
    verilog = open(instance+'.v' , 'r')

    print('###################\nModule: '+instance+'\n###################')
    extract_overview(bsv)
    extract_module(bsv)
    extract_compile_macros(bsv)
    #print('\nLibrary Imports'+'\n^^^^^^^^^^^^^^^^^^')
    #extract_imports(bsv)
    extract_regs(bsv)
    extract_wires(bsv)
    extract_fifos(bsv)
    extract_funcs(bsv)
    extract_ifc(bsv)
    extract_rules(bsv, sched)
    extract_notes(bsv)

    bsvfile.close()
    schedule.close()
    use.close()
    verilog.close()
