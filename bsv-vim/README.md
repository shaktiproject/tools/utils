# VIM for BSV

## Quickstart

```
mkdir -p ~/.vim/ftdetect
cp ftdetect/* ~/.vim/ftdetect

mkdir -p ~/.vim/indent
cp indent/* ~/.vim/indent

mkdir -p ~/.vim/syntax
cp syntax ~/.vim/syntax

cp lic.txt ~/.vim/

cat vimrc >> ~/.vimrc
```

## Features of the vimrc

1. Each time you open a .bsv file, the lic.txt is appended at the top of the file. So , you need to update the lic.txt with your name and email. You can also change the default imports in the same file.
2. The vimrc supports [persistent undo](https://jovicailic.org/2017/04/vim-persistent-undo/).
3. For bsv the text-width and color-column is set to 100.
4. To browse through tabs you can use `ctrl+L` or `ctrl+R`
5. You can move tabs by using `alt+L` or `alt+R`
6. Beautify Command:
    Executing the following in a vim session will ensure that spaces are inserted before and after arithmetic ops, assignment statements and logical ops, if not already present
    ` :Beautify`
7. Beautify1 Command:
    Executing the following in a vim session will ensure that spaces are inserted after all commas if not already present
    ` :Beautify1`
8. Several macro expansions are also supported. Please refer to the vimrc section for a quick glance.


