from common.log import logger
import argparse
import re
import yaml
import os
import errno
import shutil
import subprocess
import glob

block = {
    'module_name': '',
    'input_count': 0,
    'output_count': 0,
    'inputs':[],
    'outputs':[]
}

class full_path(argparse.Action):
    """Expand user- and relative-paths"""
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, os.path.abspath(os.path.expanduser(values)))

def clear_block():
    block['module_name'] =  '';
    block['input_count'] =  0;
    block['output_count'] =  0;
    block['inputs'] = [];
    block['outputs'] = [];

def is_dir(dirname):
    """Checks if a path is an actual directory"""
    if not os.path.isdir(dirname):
        msg = "{0} is not a directory".format(dirname)
        raise argparse.ArgumentTypeError(msg)
    else:
        return dirname

arg_opts = argparse.ArgumentParser(description='Generate png block diagram from BSV source')
arg_opts.add_argument('-f', '--filename', required=True, help='bsv source file; \"all\" for all files in dirname')
arg_opts.add_argument('-d', '--dirname', required=True, action=full_path, type=is_dir, help='bsv directory source')
arg_opts.add_argument('-g', '--getall', required=False, action="store_true", help='bsv directory source')
args = arg_opts.parse_args()



def parse_bsv(dirname, filename):

    file = open('{0}/{1}.bsv'.format(dirname, filename),'r')
    
    interface_start=0
    for line in file:
        line = line.rstrip('\n')
        match_comment = re.match(r'\s*\/\/',line)
        match_empty = re.match(r'^\s*$',line)
        match_interface = re.match(r'\s*interface\s+(\w+?)\s*;',line)
        if match_interface:
            logger.debug('interface name: {}'.format(match_interface.group(1)))
            interface_start=1
        
        if interface_start == 1 and not(match_comment) and not(match_empty):
            match_action_rxe = re.match(r'\s*interface\s+RXe\s*#\s*\(.*\)\s*(.*)\s*;',line)
            match_action_put = re.match(r'\s*interface\s+Put\s*#\s*\(.*\)\s*(.*)\s*;',line)
            match_action_value_get = re.match(r'\s*interface\s+Get\s*#\s*\(.*\)\s*(.*)\s*;',line)
            match_action_value_txe = re.match(r'\s*interface\s+TXe\s*#\s*\(.*\)\s*(.*)\s*;',line)
            match_action_value = re.match(r'\s*method\s+ActionValue\s*\#\s*\(\s*.*\)\s*(\w+)\s*.*;',line)
            match_action = re.match(r'\s*method\s+Action\s+(\w+)\s*.*;',line)
            match_action_value_mv = re.match(r'\s*method\s+.*\s+(mv\_\w+)\s*;',line)
            match_action_value_misc = re.match(r'\s*method\s+.*\s+(\w+)\s*;',line)
            if match_action_rxe:
                logger.debug('input found: {}'.format(match_action_rxe.group(1)))
                block['input_count'] = block['input_count'] + 1
                block['inputs'].append(match_action_rxe.group(1))
            elif match_action_put:
                logger.debug('input found: {}'.format(match_action_put.group(1)))
                block['input_count'] = block['input_count'] + 1
                block['inputs'].append(match_action_put.group(1))
            elif match_action_value_get:
                logger.debug('output found: {}'.format(match_action_value_get.group(1)))
                block['output_count'] = block['output_count'] + 1
                block['outputs'].append(match_action_value_get.group(1))
            elif match_action_value_txe:
                logger.debug('output found: {}'.format(match_action_value_txe.group(1)))
                block['output_count'] = block['output_count'] + 1
                block['outputs'].append(match_action_value_txe.group(1))
            elif match_action_value:
                logger.debug('output found: {}'.format(match_action_value.group(1)))
                block['output_count'] = block['output_count'] + 1
                block['outputs'].append(match_action_value.group(1))
            elif match_action:
                logger.debug('input found: {}'.format(match_action.group(1)))
                block['input_count'] = block['input_count'] + 1
                block['inputs'].append(match_action.group(1))
            elif match_action_value_mv:
                logger.debug('output found: {}'.format(match_action_value_mv.group(1)))
                block['output_count'] = block['output_count'] + 1
                block['outputs'].append(match_action_value_mv.group(1))
            elif match_action_value_misc:
                logger.debug('output found: {}'.format(match_action_value_misc.group(1)))
                block['output_count'] = block['output_count'] + 1
                block['outputs'].append(match_action_value_misc.group(1)) 
        
        match_endinterface = re.match(r'\s*endinterface\s*',line)
        if match_endinterface:
            interface_start=0
        
        match_module = re.match(r'\s*module\s+(\w+).*',line)
        if match_module:
            logger.debug('module found: {}'.format(match_module.group(1)))
            block['module_name'] = match_module.group(1)
    
    file.close()
    
    with open('{}.yaml'.format(block['module_name']), 'w') as fp:
        yaml.dump(block,fp)
    return block['module_name']

def gen_digraph(module_name):

    with open('{}.yaml'.format(module_name), 'r') as stream:
        block = yaml.safe_load(stream)
    
    dot_file = open('{}.dot'.format(module_name), 'w')
    dot_file.write('digraph G {\n')
    dot_file.write('\trankdir=LR;\n')
    dot_file.write('\tsplines=\"line\";\n')
    #dot_file.write('\tbgcolor=\"transparent\";\n')
    color = '#DAE8FC'
    arrow_color = '#303E6B'
    port_count = max(block['input_count'], block['output_count'])
    module_name_pos = int(port_count/2) - 1 
    port_string = '';
    for i in range(port_count):
        if i == module_name_pos:
            if i == 0:
                port_string = '<port{0}> {1} '.format(i, block['module_name'])
            else:
                port_string = port_string + '| <port{0}> {1} '.format(i, block['module_name'])
        else :
            if i == 0: 
                port_string = '<port{}> '.format(i)
            else:
                port_string = port_string + '| <port{}>'.format(i)

    dot_file.write('\tmodule [label=\"{}\",\n'.format(port_string))
    dot_file.write('\t\tshape=record, style=filled, color=\"{0}\", fillcolor=\"{0}\"\n'.format(color))
    dot_file.write('\t\twidth=2, height={}];\n'.format(port_count))
    for i in range(block['input_count']):
        dot_file.write('\tinput_intf{} [style=\"invis\"];\n'.format(i))
    
    for i in range(block['output_count']):
        dot_file.write('\toutput_intf{} [style=\"invis\"];\n'.format(i))  

    for i in range(block['input_count']):
        dot_file.write('\tinput_intf{0} -> module:port{0} [label=\"{1}\", penwidth=3.0, color=\"{2}\"];\n'.format(i,block['inputs'][i], arrow_color))

    for i in range(block['output_count']):
        dot_file.write('\tmodule:port{0} -> output_intf{0} [label=\"{1}\", penwidth=3.0, color=\"{2}\"];\n'.format(i,block['outputs'][i], arrow_color))
    dot_file.write('}\n')


def mkdir_p(path):
    """ 'mkdir -p' in Python """
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def setup(filename):
    work_path = 'workdir/{}'.format(filename)
    shutil.rmtree(work_path, ignore_errors=True)
    mkdir_p(work_path)
    os.chdir(work_path)
    logger.info('Gen dia: {}'.format(work_path))

def main():
    pwd = os.environ['PWD']
    shutil.rmtree('workdir/dia', ignore_errors=True)
    if args.filename == 'all':
        bsv_files = [x for x in os.listdir(args.dirname) if x.endswith('.bsv')]
        for bsv_f in bsv_files:
            dot_index = bsv_f.index('.')
            bsv_f = bsv_f[:dot_index]
            setup(bsv_f)
            module_name = parse_bsv(args.dirname, bsv_f)
            gen_digraph(module_name)
            subprocess.run('dot -Tpng {0}.dot -o {0}.png'.format(module_name).split(), stdout=subprocess.PIPE)
            os.chdir(pwd)
            clear_block()
    else:
        setup(args.filename)
        module_name = parse_bsv(args.dirname, args.filename)
        gen_digraph(module_name)
        subprocess.run('dot -Tpng {0}.dot -o {0}.png'.format(module_name).split(), stdout=subprocess.PIPE)
        os.chdir(pwd)
        clear_block()

    if args.getall:
        files = glob.glob('workdir/*/*.png')
        mkdir_p('workdir/dia')
        for f in files:
            shutil.move(f, 'workdir/dia')

if __name__== "__main__":
  main()

