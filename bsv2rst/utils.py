import pathlib
import logging
import argparse
import operator

class ColoredFormatter(logging.Formatter):
    """
        Class to create a log output which is colored based on level.
    """

    def __init__(self, *args, **kwargs):
        super(ColoredFormatter, self).__init__(*args, **kwargs)
        self.colors = {
            'DEBUG': '\033[94m',
            'INFO': '\033[92m',
            'WARNING': '\033[93m',
            'ERROR': '\033[91m',
        }

        self.reset = '\033[0m'

    def format(self, record):
        msg = str(record.msg)
        level_name = str(record.levelname)
        name = str(record.name)
        color_prefix = self.colors[level_name]
        return '{0}{1:<9s} : {2}{3}'.format(color_prefix,
                                            '[' + level_name + ']', msg,
                                            self.reset)


def setup_logging(log_level):
    """Setup logging

        Verbosity decided on user input

        :param log_level: User defined log level

        :type log_level: str
    """
    numeric_level = getattr(logging, log_level.upper(), None)

    if not isinstance(numeric_level, int):
        print(
            "\033[91mInvalid log level passed. Please select from debug | info | warning | error\033[0m"
        )
        raise ValueError("{}-Invalid log level.".format(log_level))

    logging.basicConfig(level=numeric_level)


class SortingHelpFormatter(argparse.HelpFormatter):

    def add_arguments(self, actions):
        actions = sorted(actions, key=operator.attrgetter('option_strings'))
        super(SortingHelpFormatter, self).add_arguments(actions)


def bsv2rst_cmdline_args():
    parser = argparse.ArgumentParser(
        formatter_class=SortingHelpFormatter,
        prog="bsv2rst",
        description="BSV2RST")
    parser.add_argument('--version',
                        '-v',
                        help='Print version of BSV2RST being used',
                        action='store_true')
    parser.add_argument('--package',
                        '-p',
                        type=str,
                        metavar='filename',
                        help='Name of the package')
    parser.add_argument('--instance',
                        '-i',
                        type=str,
                        metavar='modulename',
                        help='Name of the module instance')
    parser.add_argument('--link',
                        '-l',
                        type=str,
                        metavar='url',
                        help='URL where the file is hosted')
    parser.add_argument('--outfile',
                        '-o',
                        type=str,
                        metavar='filename',
                        help='Name of the file where the RST should be written')
    parser.add_argument('--dir',
                        '-d',
                        type= lambda p: str(pathlib.Path(p).absolute()),
                        metavar= 'PATH',
                        default=str(pathlib.Path('.').absolute()),
                        help='Path of bsv file'
                        )
    parser.add_argument('--config',
                        '-c',
                        type=str,
                        metavar='STRING',
                        default='InReFiFuWiRuNo',
                        help='''String to indicate what should be dumped\n
                                In: Interfaces
                                Re: Registers
                                Fi: FIFOs
                                Fu: Local Functions
                                Wi: Wires
                                Ru: Rules
                                N : Notes'''
                        )
    parser.add_argument('--verbose',
                        action='store',
                        default='info',
                        help='debug | info | warning | error',
                        metavar="")
    return parser
