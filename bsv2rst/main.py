import logging
import os
import sys
import shutil
import bsv2rst.bsv2rst as bsv2rst
import bsv2rst.utils as utils
import re
from bsv2rst import __version__ as version

logger = logging.getLogger(__name__)

def main():

    parser = utils.bsv2rst_cmdline_args()
    args = parser.parse_args()
    
    if args.version :
        print('BSV2RST: BSV documentation in RST format')
        print('Version: ' + version)
        return 0

    utils.setup_logging(args.verbose)
    logger = logging.getLogger()
    logger.handlers = []
    ch = logging.StreamHandler()
    ch.setFormatter(utils.ColoredFormatter())
    logger.addHandler(ch)
    fh = logging.FileHandler('run.log', 'w')
    logger.addHandler(fh)

    global srclink
    srclink = args.link+'#'
    bsvfile = open(args.dir+'/'+args.package+'.bsv', 'r')
    bsv = bsvfile.read()
    rst = open(args.outfile,'w')
    instance = args.instance



    schedule = open(args.dir+'/mk'+instance+'.sched' , 'r')
    sched = schedule.read()
    
    gen_rst_file(bsv, rst, instance, sched, srclink, args.config)
    
    bsvfile.close()
    rst.close()
    schedule.close()


def gen_rst_file(bsv, rst, instance, sched, srclink, config):
    rst.write('###################\nModule: '+instance+'\n###################\n\n')
    logger.debug('Extracting Overview')
    bsv2rst.extract_overview(bsv,rst)
    logger.debug('Extracting Compile Macros')
    bsv2rst.extract_compile_macros(bsv, rst)
    logger.debug('Extracting Imports')
    bsv2rst.extract_imports(bsv, rst)
    if 'In' in config:
        logger.debug('Extracting Interfaces')
        bsv2rst.extract_ifc(bsv, rst, instance, srclink)

    if 'Re' in config:
        logger.debug('Extracting Registers')
        bsv2rst.extract_regs(bsv, rst, srclink)
    if 'Fi' in config:
        logger.debug('Extracting FIFOs')
        bsv2rst.extract_fifos(bsv, rst, srclink)
    if 'Fu' in config:
        logger.debug('Extracting Functions')
        bsv2rst.extract_funcs(bsv, rst, srclink)
    if 'Wi' in config:
        logger.debug('Extracting Wires')
        bsv2rst.extract_wires(bsv, rst, srclink)
    if 'Ru' in config:
        logger.debug('Extracting Rules')
        bsv2rst.extract_rules(bsv, sched, rst, srclink)
    if 'No' in config:
        logger.debug('Extracting Notes')
        bsv2rst.extract_notes(bsv, rst, srclink)
    logger.debug('Generating interface image')
    bsv2rst.gen_dot_image(bsv, rst, instance)

if __name__ == "__main__":
    exit(main())
